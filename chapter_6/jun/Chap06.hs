-- ex01.
fac 0 = 1
fac n | n > 0 = n * fac (n-1)

-- ex02.
sumdown 0 = 0
sumdown n = n + sumdown (n-1)

-- ex03.
{--
(^) :: Int -> Int -> Int
m ^ 0 = 1
m ^ n = m * (m ^ (n-1))

2 ^ 3
={ applying ^ }
2 * (2 ^ 2)
={ applying ^ }
2 * (2 * (2 ^ 1))
={ applying ^ }
2 * (2 * (2 * (2 ^ 0)))
={ applying ^ }
2 * (2 * (2 * 1))
={ applying * }
8
--}

-- ex04.
euclid x y | x == y = x
           | x < y = euclid x (y-x)
           | y < x = euclid (x-y) y

-- ex05.
-- length' [1,2,3]
-- 1 + (length' [2,3])
-- 1 + 1 + (length' [3])
-- 1 + 1 + 1 + (length' [])
-- 1 + 1 + 1 + 0

-- drop' 1 [1,2,3]
-- drop' 0 [2,3]
-- [2,3]

-- init' [1,2,3]
-- 1 : init' [2,3]
-- 1 : 2 : init' [3]
-- 1 : 2 : []
-- [1,2]

-- ex06.
and' :: [Bool] -> Bool
and' [] = True
and' (x:xs) | x == False = False 
            | otherwise = and' xs

concat' :: [[a]] -> [a]
concat' []       = []
concat' (xs:xss) = xs ++ concat' xss

replicate' :: Int -> a -> [a]
replicate' 0 _ = []
replicate' n x = x : replicate' (n-1) x

{--
(!!) :: [a] -> Int -> a
(!!) (x:xs) 0 = x
(!!) (x:xs) n = (!!) xs (n-1)
--}

elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' k (x:xs) | k == x = True
               | otherwise = elem' k xs

-- ex. 07
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) 
  | x < y     = x : merge xs (y : ys)
  | otherwise = y : merge (x : xs) ys

-- ex. 08
halve :: [a] -> ([a], [a])
halve xs = ((take n xs), (drop n xs))
    where n = length xs `div` 2

-- ex. 09
sum' :: [Int] -> Int
sum' [] = 0
sum' (x:xs) = x + (sum' xs)

take' :: Int -> [a] -> [a] 
take' 0 _  = []
take' _ [] = []
take' n (x:xs) = x : take' (n-1) xs 

last' :: [a] -> a
last' [x] = x
last' (x:xs) = last' xs
