-- 6.1. Basic Concepts
fac0 :: Int -> Int
fac0 n = product [1..n]

fac1 :: Int -> Int
fac1 0 = 1
fac1 n = n * fac1 (n-1)

fac2 3 = 1
fac2 n = n * fac2 (n-1)

{--
(*) :: Int -> Int -> Int
m * 0 = 0; m * n = m + (m * (n - 1))
--}

-- 6.2. Recursion on lists
-- 2:[3,4]
-- 2:(3:(4:[]))
product' :: Num a => [a] -> a
product' [] = 1
product' (n:ns) = n * product' ns

length' :: [a] -> Int
length' []= 0
length' (_:xs) = 1 + length' xs

reverse' :: [a] -> [a]
reverse' []= []
reverse' (x:xs) = reverse' xs ++ [x]

{--
(++) :: [a] -> [a] -> [a]
[]++ ys = ys; (x:xs) ++ ys = x : (xs ++ ys)
--}

insert :: Ord a => a -> [a] -> [a]
insert x []  = [x]
insert x (y:ys) | x <= y = x : y : ys
                | otherwise = y : insert x ys

isort :: Ord a => [a] -> [a]
isort [] = []
isort (x:xs) = insert x (isort xs)

-- 6.3. Multiple arguments
zip' :: [a] -> [b] -> [(a,b)]
zip' [] _ = []
zip' _ [] = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys

drop' :: Int -> [a] -> [a]
drop' 0 xs = xs
drop' _ [] = []
drop' n (_:xs) = drop' (n-1) xs

-- 6.4. Multiple recursion
fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n-2) + fib (n-1)

-- 6.5. Mutual recursion
even' :: Int -> Bool
even' 0 = True
even' n = odd' (n-1)

odd' :: Int -> Bool
odd' 0 = False
odd' n = even' (n-1)

evens :: [a] -> [a]
evens [] = []
evens (x:xs) = x : odds xs

odds :: [a] -> [a]
odds [] = []
odds (_:xs) = evens xs

-- 6.6 Advice on recursion
product'' :: [Int] -> Int
product'' [] = 1
prodcut'' (n:ns) = n * product'' ns

product''' :: Num a => [a] -> a
product''' = foldr (*) 1

drop'' :: Int -> [a] -> [a]
--drop'' 0 [] = []
--drop'' 0 (x:xs) = x:xs
drop'' 0 xs = xs
drop'' n [] = []
drop'' n (x:xs) = drop'' (n-1) xs

drop''' :: Integral b => b -> [a] -> [a]
dorp''' 0 xs = xs
drop''' n [] = []
drop''' n (x:xs) = drop''' (n-1) xs

drop'''' :: Int -> [a] -> [a]
dorp'''' 0 xs = xs
drop'''' _ [] = []
drop'''' n (_:xs) = drop'''' (n-1) xs

init' :: [a] -> [a]
init' (x:xs) | null xs = []
             | otherwise = x : init' xs

init'' :: [a] -> [a]
init'' [_] = []
init'' (x:xs) = x : init'' xs

