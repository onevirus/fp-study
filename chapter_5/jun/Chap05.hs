module Chap05
    ( ex01
    ) where


-- 1.
sum [x^2 | x <- [1..100]]

-- 2.
--grid :: Int -> Int -> [(Int, Int)];
grid m n = [(x, y) | x <- [0..m], y <- [0..n]]

-- 3.
--square :: Int -> [(Int, Int)];
square n = [(x, y) | (x, y) <- grid n n, x /= y]

--4.
--replicate :: Int -> a -> [a];
replicate n x = [x | _ <- [1..n]]

-- 5.
--pyths :: Int -> [(Int, Int ,Int)];
pyths n = [(x, y, z) | x <- [1..n], y <- [1..n], z <- [1..n], (x^2) + (y^2) == (z^2)]


-- 6.
--factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]

--perfects :: Int -> [Int]
perfects n = [x | x <- [1..n], sum (factors x) == 2 * x]


-- 7.
--ex07 = [(x, y) | x <- [1,2], y <- [3,4]]


-- 8.
find k t = [v | (k',v) <- t, k == k']
positions x xs = find x [(k,v) | (k,v) <- zip xs [0..]]


-- 9.
scalarproduct xs ys = sum [x*y | (x,y) <- zip xs ys]


-- 10.
import Data.Char
let2intLower c = ord c - ord 'a'
let2intUpper c = ord c - ord 'A'

int2letLower n = chr (ord 'a' + n)
int2letUpper n = chr (ord 'A' + n)

:{
shift n c
    | isLower c = int2letLower ((let2intLower c + n) `mod` 26)
    | isUpper c = int2letUpper ((let2intUpper c + n) `mod` 26)
    | otherwise = c

:}

encode n xs = [shift n x | x <- xs]

:{
table = [8.1, 1.5, 2.8, 4.2, 12.7, 2.2, 2.0, 6.1, 7.0,
         0.2, 0.8, 4.0, 2.4, 6.7, 7.5, 1.9, 0.1, 6.0,
         6.3, 9.0, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1]
tables = table ++ table
:}

percent n m = (fromIntegral n / fromIntegral m) * 100

alphas xs = length [x | x <- xs, x >= 'A' && x <= 'z']
freqs xs = [percent (count x xs) n | x <- ['A'..'z']] where n = alphas xs

chisqr os es = sum [((o-e)^2)/e | (o,e) <- zip os es]
rotate n xs = drop n xs ++ take n xs

:{
crack xs = encode (-factor) xs
    where
        factor = head (positions (minimum chitab) chitab)
        chitab = [chisqr (rotate n table') tables | n <- [0..25*2]]
        table' = freqs xs
:}