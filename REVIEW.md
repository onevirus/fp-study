8월 1일 

# 왜 스터디 ? 
함수형 언어를 배우고 싶다.

# 왜 함수형 ? 
코드의 간결함. 다른 패러다임의 언어는 실무에서 많이 써서 따로 배울 필요가 없다.
https://ko.wikipedia.org/wiki/에라토스테네스의_체

# 왜 하스켈 ? 
타협하지 않는 순수 함수형 언어. 하스켈을 배우면 다른 언어도 잘 하게 된다. 코드의 우아함.

# 누가 ?
Bro, Jay, Jerry, Jun, Martin

# 언제 ?
5/13 ~ 7/15 매주 월요일

# 어떻게 ?
Programming in Haskell 한 챕터씩
2시간 정도씩 모여서 30분 설명, 1시간 연습문제 코딩

# 무엇을 ?
* Chap 1 ~ 9
* 하스켈 문법
* 타입 추론
* 패턴 매칭
* 커링
```haskell
mult x y z = x * y * z
```
* 람다
```
\x -> x + x
add x y = x + y
add = \x -> (\y -> x + y)
```
* List comprehension   
{x^2 | x ∈ {1 . . 5}}
```
[x^2 | x <- [1..5]]
```
* Recursive type
* Higher-order function

# 도움이 된 점
* Python 언어에 지대한 영향을 준 Haskell 언어를 익힘으로써 Python 코드도 더 잘 짤 수 있게 되었다
* 함수형 프로그래밍 스타일로 코드를 짜면 간결하게 짤 수 있다
* 최대한 사이드 이펙트가 없는 코드를 짜려고 하는 자신을 발견할 수 있다
* 다양한 분야, 그룹의 멤버들과 함께 배울 수 있다
* 평소 혼자 공부하기 힘든 하스켈을 배우고 토론할 수 있어 즐거웠다.
* 실무에서 접하기 어려운 함수형 프로그래밍을 배울 수 있었다.

# 함수형 스터디 2기
* 8월 마지막주 휴가 끝나고 시작
* Monad
* Monadic parser
* Lazy evaluation