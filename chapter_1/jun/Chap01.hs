module Chap01
    ( ex01, ex02, ex03, ex04, ex05
    ) where

ex01 :: IO ()
double x = x + x
ex01 = print(double 2)

ex02 :: IO ()
ex02 = print(sum[] + sum[1])

ex03 :: IO ()
--product' :: Num a => [a] -> a
product'[] = 1
product'(x:xs) = x * product' xs
ex03 = print(product'[2, 3, 4])

ex04 :: IO ()
revQsort [] = []
revQsort (x:xs) = revQsort larger ++ [x] ++ revQsort smaller
                  where
                    smaller = [a | a <- xs, a <= x]
                    larger  = [b | b <- xs, b > x]
ex04 = print(revQsort[2, 3, 4])

ex05 :: IO ()
uniqueQsort [] = []
uniqueQsort (x:xs) = uniqueQsort smaller ++ [x] ++ uniqueQsort larger
                  where
                    smaller = [a | a <- xs, a < x]
                    larger  = [b | b <- xs, b > x]
ex05 = print(uniqueQsort[2, 2, 3, 1, 1])

