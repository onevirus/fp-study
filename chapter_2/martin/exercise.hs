double x = x + x
quadruple x = double (double x)
-- Factorial of a positive integer:
factorial n = product [1..n]
-- Average of a list of integers:
average ns = sum ns `div` length ns

-- layout rule
a = b + c
  where
    b = 1
    c = 2
d = a * 2

{-
 - this is multiline comment
 -}

nn = a `div` (length xs)
  where
    a = 10
    xs = [1,2,3,4,5]

mylast xs = head (reverse xs)

myinit xs = take (length xs - 1) xs