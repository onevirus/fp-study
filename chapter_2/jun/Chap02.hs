module Chap02
    ( prac01, prac02, prac03, ex03, ex04, ex05
    ) where

--prac01 :: IO ()
average ns = sum ns `div` length ns
prac01 = print( average [1..10])

--prac02 :: IO ()
{-
comment
-}
a = b + c
    where
        b = 1
        c = 2
prac02 = print(a)

--prac03 :: IO ()
factorial n = product [1..n]
prac03 = print (factorial 10)

-- ex02
-- (2)^3*4
-- (2*3) + (4*5)
-- 2 + 3*(4^5)

n = a `div` length xs
    where
        a = 10
        xs = [1,2,3,4,5]
ex03 = print (n)

-- ex04
x = [1,2,3,4,5]
last' n = head (reverse x)
ex04 = print(last' x)

-- ex05
getN1 ns = take (length x - 1) x
ex05 = print(getN1 x)