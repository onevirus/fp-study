{-
module Chap03
    ( ex01, ex02
    ) where

import Data.Typeable

ex01 = print(typeOf ['a', 'b', 'c'])
add' a b c d = a + b + c + d
ex02 = print(add' 1 2 3 4)
-}


-- 1.
-- ['a', 'b', 'c']
-- ['a', 'b', 'c'] :: [Char]

-- [(False, '0'), (True, '1')]
-- [(Bool, Char)]

-- ([False, True], ['0', '1'])
-- ([Bool], [Char])

-- [tail, init, reverse]
-- [[a] -> [a]]

-- 2.
bools = [True]
--bools :: [Bool]

nums = [[1..3::Int]]
--nums :: [[Int]]

add a b c = a + b + c::Int
--add :: Int -> Int -> Int -> Int

copy x = (x, x)
--copy :: a -> (a, a)
--copy :: b -> (b, b)

apply x y = x y
--apply :: (a -> b) -> a -> b
--apply :: (t1 -> t2) -> t1 -> t2

-- 3.
second xs = head (tail xs)
--second :: [a] -> a

swap (x, y) = (y, x)
--swap :: (a, b) -> (b, a)

pair x y = (x, y)
--pair :: a -> b -> (a, b)

double x = x + x
--double :: Num a => a -> a

palindrome xs = reverse xs == xs
--palindrome :: Eq a => [a] -> Bool

twice f x = f (f x)
--twice :: (t -> t) -> t -> t

-- 5.
-- input type에 따라 달라짐