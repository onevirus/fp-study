-- Question 1
['a', 'b', 'c']::[Char]
('a', 'b', 'v')::(Char)
[(False, 'F'), (True, 'T')]::[(Bool, Char)]
([False, True], ['F', 'T'])::([Bool, Char])
[tail, head, init]::[[a]->[a]]

-- Question 2
bools = [True, True, True]
nums = [[1,2,3]]
add a b c = a + b + c
copy a = (a,a)
apply f x = f x

-- Question 3
:type second xs = head (tail xs) 	 -- returns the second element in a list of type x
:type swap (x,y) 		  	 -- returns tuple of type y,x
:type pair x y = (x,y)			 -- returns tuple of type x,y
:type double x = x*2			 -- returns type x, doesn't generalize for Lists & Tuples
:type palindrome xs = reverse xs == xs	 -- returns bool
:type twice f x = f (f x) 		 -- returns 

-- Question 4

-- Question 5
-- Functions are deemed unfit to be instances of th Eq class as many functions are overloaded with various types
-- this means that same function might have different types at runtime, making it hard to define them as Eq class instances 

