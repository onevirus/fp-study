# fp-study

Functional programming study

## 5/20 chapter 2

Glasgow Haskell Compiler   
  GHC, GHCi

Standard prelude   
  built-in functions   
  list - head, tail, !!, take, drop, length, sum, product, ++, reverse

Function application   
  denoted by space

Haskell scripts   
  .hs suffix

Naming requirements   
  begin with a lower-case letter   
  ns, xs, css

The layout rule   
  indent with spaces

No Tabs

Comment   
  --, {-  -}