--
twice f x = f (f x)

-- 2.c
takeWhile' p [] = []
takeWhile' p (x:xs) | p x = x : takeWhile' p xs
                    | otherwise = []
-- 2.d
dropWhile' p [] = []
dropWhile' p (x:xs) | p x = dropWhile' p xs
                    | otherwise = x : dropWhile' p xs
-- 3
map'' f = foldr (\x xs -> f x : xs) []
-- 4
dec2int' :: [Int] -> Int
dec2int' = foldl (\x -> \y -> 10*x + y) 0
-- 5
curry' f (x, y) = \x -> f(x, y)
~