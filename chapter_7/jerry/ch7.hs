--2.c
takeWhile _ [] = []
takeWhile f (x:xs) | f x  = x : takeWhile f xs
                   | otherwise = []


--2.d
dropWhile _ [] = []
dropWhile f (x:xs) | f x  = dropWhile f xs
                   | otherwise = x:xs                   

--7.3
map f = foldr (\x ys -> f x : ys) []
filter f = foldr (\x ys -> if f x then x:ys else ys) []


--7.4
dec2int = foldl(\a b -> a * 10 + b) 0

