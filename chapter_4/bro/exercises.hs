-- Q1
halve xs = (take n xs, drop n xs)
            where n = length xs `div` 2

-- Q2
third1 xs =  head (tail (tail xs))
third2 xs = xs !! 2
third3 (_:_:x:_) = x

-- Q3 Fail-safe tail using

--1. conditional expression
safetail1 xs = if length xs == 0 then [] else tail xs

-- 2. guarded equations 
safetail2 xs | ((length xs) == 0) = []  
             | otherwise = tail xs

-- 3. pattern matching
safetail3 [] = []
safetail3 (_:xs) = xs

{-- Q4
 a || b | a == b = a
        | otherwise True

True || True == True
True || False == True
False || True == True
False || False == False
--}

-- Q5
(&&) a b = if a then 
            (if b then True else False ) 
            else False

-- Q6
-- (&&) a b = if

-- Q7
mult = \x -> (\y -> (\z -> (x * y * z)))


-- Q8
luhnDouble :: Int -> Int
luhnDouble x = if x > 9 then (x * 2) - 9 else x

luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = (((luhnDouble a) + b + (luhnDouble c) + d) `mod` 10) == 0