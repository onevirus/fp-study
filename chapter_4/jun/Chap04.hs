module Chap04
    ( havle--, ex02
    ) where


True && True = True
_ && _ = False


-- 1.
calLen xs = (length xs) `div` 2
selFirstHalf xs = take (calLen xs) xs
selLastHalf xs = drop (calLen xs) xs
havle xs = (selFirstHalf xs, selLastHalf xs)

-- 2.
-- a.
ex02a xs = head ( tail ( tail xs ) )

-- b.
ex02b xs = xs !! 2

-- c.
ex02c (_:_:x:xs) = x


-- 3.
-- a.
ex03a xs = if null xs then [] else tail xs

-- b.
ex03b xs | null xs   = []
         | otherwise = tail xs

-- c.
ex03c [] = []
ex03c xs = tail xs

-- 4.
True || _ = True
_ || True = True

-- 5.
a && b = if a == True
           then if b == True
                then True
                else False
           else False

-- 6.
a && b = if a == True then b else False

-- 7.
ex07 = \x -> (\y -> (\z -> x * y * z))

-- 8.
luhnDouble x | x * 2 > 9 = (x * 2) - 9
             | otherwise = x * 2

luhn a b c d = (((luhnDouble a) + b + (luhnDouble c) + d) `mod` 10) == 0
