-- 1
halve :: [a] -> ([a],[a])
halve nx = splitAt ((length nx) `div` 2) nx

-- 2
-- a
third' nx = head (tail (tail nx))
-- b
third'' nx = nx !! 2
-- c
-- third''' :: [a] -> a
third''' (_:_:a:_) = a

-- 3
-- a
safetail' nx = if null nx then nx else tail nx
-- b
safetail'' nx | null nx = nx | otherwise = tail nx
-- c
safetail''' (_:b) = b
safetail''' a = a

-- 4
(||) :: Bool -> Bool -> Bool
True || _ = True
False || b = b

-- 5
(&&) a b = if a == True then if b == True then True else False else False

-- 6
(&&&) a b = if a == True then b else False

-- 7
-- mult' :: Int -> (Int -> Int)
mult' = \x -> (\y -> (\z -> z*y*x) )

-- 8
luhnDouble a = if a*2 > 9 then a*2 - 9 else a*2
luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = (((luhnDouble a) + b + (luhnDouble c) + d) `mod` 10) == 0